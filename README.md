# Colonial Expansion

https://www.curseforge.com/minecraft/modpacks/colonial-expansion

![](http://i.imgur.com/b5WBVxj.png)

## Description
A few moments ago you were in the lap of luxury talking to the Council of the Galactic Accord about your plans to colonize remote planets and how you can do it so much better than the terraforming robots they currently use. As a test they have beamed you down to a back-world class M planet that has recently recovered from a Minecraft addiction. They have strategically wiped your memory of your ability to craft advanced technology although you can still dream of and imagine those contraptions. The last you thing you remember is their challenge that you must travel this hostile world  at least 1km before starting your colony, that punching trees yield less than ideal resources, and that even the most common of materials have a longer crafting chain. Oh, and that other brave souls might be joining you.

 

... BREAKING NEWS ... [breaking news sounds here]

 

The Galactic council has recently decided to allow you to build computers, robots and a few mid-level tech items considering that the restrictions are so tight. This they feel would be an excellent test of your skill to build a colony under harsh conditions. 

 

## Requirements
### Ram: 
2.5 GB Minimum || 5 GB Recommended
### Java: 
8 u121 or better

 